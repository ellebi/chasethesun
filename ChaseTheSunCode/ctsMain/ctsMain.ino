// NOTE: The code is commented in Italian. An English version might be available in the future.

// LED di bordo
#define LED 1

// Pin per il controllo del ponte-H
#define HBC_1 16
#define HBC_2 17  // sinistra
#define HBC_3 5
#define HBC_4 18  // destra

// Pin per l'abilitazione del ponte-H
#define HBE_LT 4
#define HBE_RT 19

// Pin per sensori
#define PIN_PHOTOCELL_FW 25
#define PIN_PHOTOCELL_FWRT 26
#define PIN_PHOTOCELL_FWLT 33
#define PIN_PHOTOCELL_RT 27
#define PIN_PHOTOCELL_LT 32
#define PIN_PANEL 13
// Dalla board si inizia da G32 in poi (andando verso il pin V5),
// sulla macchina corrispondono in ordine dal sensore di sinistra in poi, verso destra.
// Il pannello solare è a parte ed è il GPIO più vicino al V5.

// PWM
#define PWM_CHAN 0          // Canale del ESP32 da usare per il PWM unico, IN ALTERNATIVA al PWM per ogni motore
#define PWM_RES 10          // Risoluzione in bit per il duty cycle
#define PWM_FREQ 10000000   // Frequenza PWM in Hz.

// Dizionario direzioni (non tutte sono per forza usate)
#define FW 100
#define FWRT 101
#define FWLT 102
#define RT 103
#define LT 104
#define BKRT 105
#define BKLT 106
#define BK 107

int sensorFW, sensorFWRT, sensorFWLT, sensorRT, sensorLT; // Sensori di illuminazione disponibili
int sensorPanel;                                          // Pannello solare (qui usato come sensore di illuminazione solare diretta)
int maxSensor;                                            // Valore del sensore più illuminato
int maxDirection;                                         // Direzione del sensore più illuminato

int panelLitThreshold;                                    // Valore del pannello solare oltre il quale la macchina viene considerata esposta al sole direttamente

// AnalogWrite presa e modificata dall'esempio "LEDCSoftwareFade", dalla libreria NodeMCU-32S.
// Arduino like analogWrite
// value has to be between 0 and valueMax
void ledcAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax)
  {
  // calculate duty, 1023 from 2 ^ 10 - 1
  uint32_t duty = (1023 / valueMax) * min(value, valueMax);   // Modifica: adattato alla risoluzione a 10 bit
  // write duty to LEDC
  ledcWrite(channel, duty);
  }

boolean RTMotorPWM(uint32_t percent)
  {
  ledcAnalogWrite(PWM_CHAN, percent, 100);
  return true;
  }

boolean LTMotorPWM(uint32_t percent)            // PWM individuale per i motori per estendibilità futura
  {
  ledcAnalogWrite(PWM_CHAN, percent, 100);
  return true;
  }
  


// Per ogni motore (1,2) e (3,4):
// (HIGH,LOW) = avanti
// (LOW,HIGH) = indietro
// (HIGH,HIGH) oppure (LOW,LOW) = freno

// I pin enable (HBE_1,HBE_2) liberano il motore corrispondente quando sono a 0,
// mentre lo sottopogono ad alimentazione (incluso freno) quando sono a 1.

boolean aheadFull (int speedPWM)          // Avanti
  {  
  digitalWrite(HBC_1, HIGH);
  digitalWrite(HBC_2, LOW);
  digitalWrite(HBC_3, HIGH);
  digitalWrite(HBC_4, LOW);

  LTMotorPWM(speedPWM);
  RTMotorPWM(speedPWM);

  return true;
  }

boolean reverseFull (int speedPWM)        // Indietro
  {  
  digitalWrite(HBC_1, LOW);
  digitalWrite(HBC_2, HIGH);
  digitalWrite(HBC_3, LOW);
  digitalWrite(HBC_4, HIGH);

  LTMotorPWM(speedPWM);
  RTMotorPWM(speedPWM);

  return true;
  }

boolean turnLeft (int speedPWM)   // Sterzo differenziale: consiste nel frenare una ruota
  {                               // per usarla come perno e avviare l'altra per creare movimento rotatorio intorno alla ruota frenata. Quindi la ruota frenate deve essere quella del lato verso cui si vuole sterzare.
  digitalWrite(HBC_1, LOW);
  digitalWrite(HBC_2, LOW);       // Frena la ruota sinistra
  digitalWrite(HBC_3, HIGH);
  digitalWrite(HBC_4, LOW);       // Avvia la ruota destra

  LTMotorPWM(speedPWM);
  RTMotorPWM(speedPWM);

  return true;
  }

boolean turnRight (int speedPWM)
  {
  digitalWrite(HBC_1, HIGH);
  digitalWrite(HBC_2, LOW);       // Avvia la ruota sinistra
  digitalWrite(HBC_3, LOW);
  digitalWrite(HBC_4, LOW);       // Frena la ruota detra

  LTMotorPWM(speedPWM);
  RTMotorPWM(speedPWM);

  return true;
  }

boolean releaseWheels ()      // Rilascia le ruote, rendendole libere di muoversi per inerzia
  {
  LTMotorPWM(0);
  RTMotorPWM(0);

  return true;
  }

boolean brakeFull (int brakePower)          // Frena entrambe le ruote
  {                                         // Probabilmente dovrebbe essere sempre usata con 100 (massimo freno)
  digitalWrite(HBC_1, LOW);
  digitalWrite(HBC_2, LOW);
  digitalWrite(HBC_3, LOW);
  digitalWrite(HBC_4, LOW);

  LTMotorPWM(brakePower);
  RTMotorPWM(brakePower);

  return true;
  }

void setup() {

  // LED
  pinMode(LED, OUTPUT);

  // Controllo motori
  pinMode(HBC_1, OUTPUT);
  pinMode(HBC_2, OUTPUT);
  pinMode(HBC_3, OUTPUT);
  pinMode(HBC_4, OUTPUT);

  // Enable motori (PWM e rilascio ruote)
  pinMode(HBE_LT, OUTPUT);
  pinMode(HBE_RT, OUTPUT);

  // Sensori
  pinMode(PIN_PHOTOCELL_FW, INPUT);
  pinMode(PIN_PHOTOCELL_FWRT, INPUT);
  pinMode(PIN_PHOTOCELL_FWLT, INPUT);
  pinMode(PIN_PHOTOCELL_RT, INPUT);
  pinMode(PIN_PHOTOCELL_LT, INPUT);
  pinMode(PIN_PANEL, INPUT);

  ledcSetup(PWM_CHAN, PWM_FREQ, PWM_RES);
  ledcSetup(PWM_CHAN, PWM_FREQ, PWM_RES);
  ledcAttachPin(HBE_LT, PWM_CHAN);
  ledcAttachPin(HBE_RT, PWM_CHAN);                      // Setup controller PWM.

  delay(500);                                           // Attendere avvio ADC etc per sicurezza - calibrazione:
  panelLitThreshold = (analogRead(PIN_PANEL) - 200);    // Impostare livello luminosità al sole, poi sottrarre un delta per ottenere
                                                        // il livello oltre il quale la macchina viene considerata esposta al sole
                                                        // direttamente (si assume che la macchina inizi al sole) <----!!!IMPORTANTE!!!
  digitalWrite(LED, LOW);
  delay(2000);
  digitalWrite(LED, HIGH);                            
  delay(2000);
  digitalWrite(LED, LOW);
  delay(2000);
  digitalWrite(LED, HIGH);                            // Questo serve ad evitare che la macchina schizzi via non appena accesa (per la calibrazione)
  
  Serial.begin(9600);                                 // La seriale si avvia alla fine per non perdere il LED in fase di setup.
}

void loop() {
  
  sensorPanel = analogRead(PIN_PANEL);
  if(sensorPanel > panelLitThreshold)
    {
    brakeFull(100);
    return;                                           // Stop quando la macchina è al sole
    }
  if(sensorPanel < 500)
    {
    brakeFull(100);
    return;                                           // Stop quando il pannello solare è totalmente coperto da qualcosa (ad es. uno straccio gettato sopra)
    }

  // Lettura fotocellule:
  sensorFW = analogRead(PIN_PHOTOCELL_FW);
  sensorFWRT = analogRead(PIN_PHOTOCELL_FWRT);
  sensorFWLT = analogRead(PIN_PHOTOCELL_FWLT);
  sensorRT = analogRead(PIN_PHOTOCELL_RT);
  sensorLT = analogRead(PIN_PHOTOCELL_LT);

  // max combinato (Arduino include solo quello a due variabili)
  maxSensor = max(max(max(sensorFW, sensorFWRT),max(sensorFWLT, sensorRT)),sensorLT);

  // NB: in caso di sensori che danno valori uguali, non si gestisce nulla separatamente.
  // Si assume che se più sensori sono uguali, allora una qualunque delle direzioni
  // corrispondenti vada bene.

  // Niente switch qui perchè richiede termini costanti.
  if (maxSensor == sensorFW) maxDirection = FW;
  if (maxSensor == sensorFWRT) maxDirection = FWRT;
  if (maxSensor == sensorFWLT) maxDirection = FWLT;
  if (maxSensor == sensorRT) maxDirection = RT;
  if (maxSensor == sensorLT) maxDirection = LT;
  // Ora maxDirection e sensorMax indicano, rispettivamente, la direzione con valore massimo e tale valore.
  
  // Cosa fare a seconda della direzione
  switch (maxDirection)
    {
    case FWRT:  turnRight(55);  break;
    case FWLT:  turnLeft(55);   break;        // Ruota lentamente se la direzione è quasi davanti
    case RT:    turnRight(65);  break;
    case LT:    turnLeft(65);   break;        // Ruota più velocemente se la direzione è molto diversa
    case FW:
      {
      if ((sensorPanel >= 500 && sensorPanel < 1000) && (panelLitThreshold > 1500)) aheadFull(80);
      else aheadFull (50);
      break;                                  // Sistema "interattivo": quando il pannello è parzialmente coperto, la macchina si muove ad alta velocità, altrimenti lentamente.
      }                                       // L'ultima condizione disabilita la funzione quando c'è troppa poca luce per poter decidere.
    default:    brakeFull(100); break;        // Questo non dovrebbe mai accadere
    }
}
