Key for reading hand-drawn schematics:

1. Photoresistors
2. Solar panel (used as sensor)
3. 12V battery
4. L298N H-Bridge (board)
5. ESP32 and breadboard
6. Wheels with DC Motor