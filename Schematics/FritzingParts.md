Please note that the following parts are required for viewing the Fritzing diagram:

SP32 NodeMCU-32S: https://forum.fritzing.org/t/esp32s-hiletgo-dev-boad-with-pinout-template/5357

L298N H-Bridge (board): https://github.com/yohendry/arduino_L298N