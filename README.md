# ChaseTheSun

ChaseTheSun is a small, wheeled robot that detects sunlit areas in its vicinity and drives to them until it is directly exposed to sunlight, then stops. It autonomously detects when it is in the shade and resumes driving to return in the sun. When its solar panel is covered (for example by a cloth), it will stop immediately until uncovered.

# Material

1. Motorized wheel-base
2. ESP32 or similar Arduino-compatible board
3. Battery for motor and board power
4. Solar panel for detecting direct exposure to sunlight
5. Photoresistors for use as light sensor
6. Resistors to construct voltage dividers
7. Breadboard and wires for connecting everything
8. Black cardboard for shading sensors from excessive light
9. Adhesive tape, staples etc. to hold everything together


# Robot behavior

The purpose of ChaseTheSun is to attempt to constantly remain in the sunlight. It accomplishes this by monitoring the light levels in its environment in an attempt to find sunlit areas, and then moves to them using its wheels. It stops when in direct sunlight, which it detects with a small solar panel. This panel does not provide actual power, instead, it is used as a direct sunlight sensor by monitoring its voltage output. As such, it needs to be small and low-powered. The solar panel is also used to immediately stop the robot when it is covered (that is, when the light level drops close to zero).


# Sensor construction

A light sensor, for the purpose of this project, is defined as a photoresistor in a black cardobard enclosure. The purpose of these sensors is to monitor the light levels of the ground to find areas illuminated by direct sunlight, to which the robot will then attempt to drive. The shape of the enclosure is crucial for defining the behavior of the sensor when it comes to detecting ground sunlight: photoresistors are not inherently directional, therefore, an unenclosed sensor would be affected by many unwanted conditions, such as the brightness of the sky, walls and other objects that are not the ground.

Work licensed under [GPL-3.0-only](https://www.gnu.org/licenses/gpl-3.0.html).